using LinearAlgebra
using MatrixMarket
using Plots
#using ImageView
#using Images

M = MatrixMarket.mmread("fidap001.mtx")

rank = 10

median = floor(Int, size(M)[1]/2)

domain1 = 1:median
domain2 = median+1:size(M)[1]

# QRCP on the first domain and the interface
A1 = M[:,domain1]
QR1 = LinearAlgebra.qr(Array(A1), Val(true))
selected_columns_1 = (transpose(collect(1:size(QR1.P)[1])) * floor.(Int, QR1.P))[1:rank]
println("Step 1 selected columns for domain 1 : ", selected_columns_1)
A1_first = M[:,selected_columns_1]

# QRCP on the second domain and the interface
A2 = M[:,domain2]
QR2 = LinearAlgebra.qr(Array(A2), Val(true))
selected_columns_2 = (transpose(collect(1:size(QR2.P)[1])) * floor.(Int, QR2.P))[1:rank]
println("Step 1 selected columns for domain 2 : ", selected_columns_2)
A2_first = M[:,selected_columns_2]

# We concatenate last selected columns of A1 and A2
# And perform QRCP
B = [A1_first A2_first]
QRB = LinearAlgebra.qr(Array(B), Val(true))
selected_columns_B = (transpose(collect(1:size(QRB.P)[1])) * floor.(Int, QRB.P))[end-rank+1:end]
selected_columns_B = [selected_columns_1 selected_columns_2][selected_columns_B]
println("Step 2 selected columns : ", selected_columns_B)

# Global indices of all rejected columns
rejected_columns = setdiff(collect(1:size(M)[1]), selected_columns_B)

AP = [M[:,selected_columns_B] M[:,rejected_columns]]
QR_global = LinearAlgebra.qr(Array(AP))

println(norm(M))
println(norm(QR_global.R[rank+1:end,rank+1:end]))

svd = LinearAlgebra.svd(Array(M))
svd_approx = LinearAlgebra.svd(QR_global.R[1:rank,:])

plot([svd.S[1:rank] svd_approx.S])

# Check the result by computing a projection matrix
#QR3 = LinearAlgebra.qr(Array(M[:,rejected_columns]))
#Q = QR3.Q[:,1:length(rejected_columns)]
#println(norm(M - Q * transpose(Q) * M))

#ImageView.imshow(colorview(Gray, Array(M)))

#sleep(120)
